import java.util.ArrayList;


public class Investments {
	private ArrayList<Investment> list;
	
	public Investments(){
		list = new ArrayList();
	}
	
	public void addAnInvestment(Investment newInvestment){
		list.add(newInvestment);
	}
	
	public Investment getInvestmentByIndex(int index){
		return list.get(index);
	}
	
	public Investment getInvestmentByNumber(int number){
		for (Investment investment : list){
			if (investment.getItemNumber() == number){
				return investment;
			}
		}
		return null;
	}
	
	public Investment deleteInvestmentByNumber(int number){
		for (Investment investment : list){
			System.out.println(investment);
			if (investment.getItemNumber() == number){
				return investment;
			}
		}
		return null;
	}
}
