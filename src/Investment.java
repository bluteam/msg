import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Investment {
	private int itemNumber = 0;
	private String itemName = "";
	private float annualReturn = 0;
	private Date annualReturnUpdated = null;
	
	public Investment(int itemNumber, String name, float newAnnualReturn){
		this.itemNumber = itemNumber;
		itemName = name;
		setAnnualReturn(newAnnualReturn);
		annualReturnUpdated = Calendar.getInstance().getTime();
	}
	
	public int getItemNumber(){
		return itemNumber;
	}

	public float getAnnualReturn() {
		return annualReturn;
	}

	public void setAnnualReturn(float annualReturn) {
		this.annualReturn = annualReturn;
		annualReturnUpdated = Calendar.getInstance().getTime();
	}

	public Date getAnnualReturnUpdated() {
		return annualReturnUpdated;
	}
	
	public void setAnnualReturnUpdated(Date newDate) {
		annualReturnUpdated = newDate;
	}
}
