import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ MSGTest.class, InvestmentsTest.class })

public class AllTests {

}
