import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;


public class FileStorageService implements StorageService{
	static final String INVESTMENTS_FILE = "investments.txt";
	private static char SEPARATOR = '#';
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat();
	private static final String DATE_FORMAT = "MM-dd-yyyy";
	
	@Override
	public ArrayList<Investment> getInvestments(ArrayList<Investment> list) throws Exception {
		Scanner scan = null;
		try
		{
			scan = new Scanner(INVESTMENTS_FILE);
			while(scan.hasNextLine())
			{
				String line = scan.nextLine();
				Investment investment = FileStorageService.parseInvestment(line);
				list.add(investment);
			}
		}
		finally
		{
			scan.close();
		}     
		return null;
	}

	@Override
	public boolean storeInvestments(ArrayList<Investment> list) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private static Investment parseInvestment(String data) throws ParseException{
		String [] fields = data.split("" + SEPARATOR);
		dateFormatter.applyPattern(DATE_FORMAT);
		int itemNumber = Integer.parseInt(fields[0]);
		String itemName = fields[1];
		float annualReturn = Float.parseFloat(fields[2]);
		Date annualReturnUpdatedDate = dateFormatter.parse(fields[3]);
		Investment investment = new Investment(itemNumber, itemName, annualReturn);
		investment.setAnnualReturn(annualReturn);
		investment.setAnnualReturnUpdated(annualReturnUpdatedDate);
		return investment;
	}
}
