import java.util.ArrayList;


public interface StorageService {
	public ArrayList<Investment> getInvestments(ArrayList<Investment> list) throws Exception;
	public boolean storeInvestments(ArrayList<Investment> list);
}
