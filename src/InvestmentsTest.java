import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class InvestmentsTest {
	private Investments investments;
	
	@Before
	public void setup() {
		investments = new Investments();
	}
	
	@After 
	public void teardown(){
		
	}
	
	@Test (expected= IndexOutOfBoundsException.class)
	public void testNoInvestment(){
		investments.getInvestmentByIndex(0);
	}
	
	@Test
	public void testAddInvestment(){
		Investment investment = new Investment(0, "test", 0 /*annual return*/);
		assertEquals(0, investment.getItemNumber());
		investments.addAnInvestment(investment);
		assertEquals(investment, investments.getInvestmentByNumber(0));
	}
	
	@Test 
	public void testModifyInvestment(){
		Investment investment = new Investment(0, "test", 0);
		assertEquals(0, investment.getItemNumber());
		investments.addAnInvestment(investment);
		investment = investments.getInvestmentByNumber(0);
		investment.setAnnualReturn(0.1f);
		assertEquals(0.1f, investment.getAnnualReturn(), 0.0001);
		Date today = Calendar.getInstance().getTime();
		assertEquals(true, 
				investment.getAnnualReturnUpdated().equals(today));
	}
}
