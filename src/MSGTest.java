import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MSGTest {
	private MSG msg;

	@Before
	public void setup() {
		msg = new MSG();
	}
	
	@After 
	public void teardown(){
		
	}
	
	@Test
	public void testNoFund(){
		int amount = msg.fundsAvaialableForWeek();
		assertEquals(0, amount);
	}
	
	@Test 
	public void testNoInvestment(){
		int income = msg.estimatedInvestIncomeForWeek();
		assertEquals(0, income);
	}
	
	@Test 
	public void testNoOperatingExpenseForWeek(){
		int expenses = msg.estimatedOperatingCostForWeek();
		assertEquals(0, expenses);
	}	
}
